package am

import (
	"context"
	"errors"
	"log"
	"net/http"
	"time"

	jsonhttp "golang.spin-2.net/alertmanagergchat/http"
)

type AMAlert struct {
	Status       string            `json:"status"` // "<resolved|firing>",
	Labels       map[string]string `json:"labels"`
	Annotations  map[string]string `json:"annotations"`
	StartsAt     time.Time         `json:"startsAt"`
	EndsAt       time.Time         `json:"endsAt"`
	GeneratorURL string            `json:"generatorURL"` // identifies the entity that caused the alert
	Fingerprint  string            `json:"fingerprint"`  // fingerprint to identify the alert
}

type AMWebhookBody struct {
	Version           string            `json:"version"`
	GroupKey          string            `json:"groupKey"`        // key identifying the group of alerts (e.g. to deduplicate)
	TruncatedAlerts   int               `json:"truncatedAlerts"` // how many alerts have been truncated due to "max_alerts"
	Status            string            `json:"status"`          // "<resolved|firing>" (TODO: make this an enum or bool-ish)
	Receiver          string            `json:"receiver"`
	GroupLabels       map[string]string `json:"groupLabels"`
	CommonLabels      map[string]string `json:"commonLabels"`
	CommonAnnotations map[string]string `json:"commonAnnotations"`
	ExternalURL       string            `json:"externalURL"` // backlink to the Alertmanager.
	Alerts            []AMAlert         `json:"alerts"`
}

// AMForwarder forwards an alertmanager webhook message to something more useful
type AMForwarder interface {
	Forward(ctx context.Context, body AMWebhookBody) error
}

// amWebhookHandler implements JSONHandler for incoming webhooks from a prometheus AlertManager
type amWebhookJSONHandler struct {
	forwarder AMForwarder
}

func (a *amWebhookJSONHandler) ServeJSON(w http.ResponseWriter, req *http.Request, reqBody AMWebhookBody) (struct{}, error) {
	log.Printf("received %+v", reqBody)
	return struct{}{}, a.forwarder.Forward(req.Context(), reqBody)
}

type fallbackHandler struct{}

func (f fallbackHandler) ServeHTTP(w http.ResponseWriter, r *http.Request, err error) {
	log.Printf("request failed to %q: %s", r.URL, err)
	switch et := err.(type) {
	case *jsonhttp.UnmarshalJSONErr:
		log.Printf("request payload: %s", string(et.Body()))
	}
	if unsucErr := (*jsonhttp.UnsuccessfulStatusErr)(nil); errors.As(err, &unsucErr) {
		log.Printf("unsuccessful status: %s; response body: %s", unsucErr.Error(), string(unsucErr.Body()))
	}
	http.Error(w, err.Error(), http.StatusInternalServerError)
}

// NewHandler creates a handler
func NewHandler(forwarder AMForwarder) http.Handler {
	wrapped := amWebhookJSONHandler{forwarder: forwarder}
	return jsonhttp.NewJSONAdapter[AMWebhookBody, struct{}](&wrapped, fallbackHandler{})
}
