package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"golang.spin-2.net/alertmanagergchat/am"
	"golang.spin-2.net/alertmanagergchat/gchat"
)

var (
	port = flag.Int("port", 0, "port number to listen on")

	webhookURL = flag.String("webhook", "", "URL to POST to with Alerts (gChat style)")
)

const amHandlerPath = "/amhandle"

func run() error {
	flag.Parse()
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer cancel()

	if *webhookURL == "" {
		return fmt.Errorf("must specify a webhook URL")
	}

	fwd, fwdErr := gchat.NewForwarder(http.DefaultClient, *webhookURL)
	if fwdErr != nil {
		return fmt.Errorf("failed to setup forwarder: %w", fwdErr)
	}

	http.Handle(amHandlerPath, am.NewHandler(fwd))

	s := http.Server{
		Addr:    ":" + strconv.Itoa(*port),
		Handler: nil, // use the default servemux
	}
	go func() {
		<-ctx.Done()
		shCtx, shCancel := context.WithTimeout(context.Background(), time.Second*3)
		defer shCancel()
		s.Shutdown(shCtx)
	}()
	srvErr := s.ListenAndServe()
	switch srvErr {
	case http.ErrServerClosed:
		return nil
	default:
		return srvErr
	}

}

func main() {
	if err := run(); err != nil {
		fmt.Fprintf(os.Stderr, "failed: %s\n", err)
		os.Exit(1)
	}
}
