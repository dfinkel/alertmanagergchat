package gchat

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"

	"golang.spin-2.net/alertmanagergchat/am"
	jsonhttp "golang.spin-2.net/alertmanagergchat/http"
)

const promFavicon = "https://prometheus.io/assets/favicons/favicon.ico"

type Forwarder struct {
	webhookURL *url.URL

	client *http.Client
}

type cardKeyVal struct {
	TopLabel         string       `json:"topLabel,omitempty"`
	Content          string       `json:"content,omitempty"`
	ContentMultiline bool         `json:"contentMultiline,omitempty"`
	BottomLabel      string       `json:"bottomLabel,omitempty"`
	Icon             string       `json:"icon,omitempty"`
	OnClick          *cardOnClick `json:"onClick,omitempty"`
}

// https://developers.google.com/chat/how-tos/cards-onclick#click_to_open_url
type cardOnClick struct {
	OpenLink struct {
		URL string `json:"url,omitempty"`
	} `json:"openLink,omitempty"`
	// skipping Action for now
}

type cardImage struct {
	ImageUrl string       `json:"imageUrl,omitempty"`
	OnClick  *cardOnClick `json:"onClick,omitempty"`
}

type cardTextParagraph struct {
	Text string `json:"text,omitempty"`
}

type cardWidget struct {
	TextParagraph *cardTextParagraph `json:"textParagraph,omitempty"`
	KeyValue      *cardKeyVal        `json:"keyValue,omitempty"`
	Image         *cardImage         `json:"image,omitempty"`
}

type cardSection struct {
	Widgets []cardWidget `json:"widgets,omitempty"`
}
type cardHeader struct {
	Title      string `json:"title,omitempty"`
	Subtitle   string `json:"subtitle,omitempty"`
	ImageUrl   string `json:"imageUrl,omitempty"`
	ImageStyle string `json:"imageStyle,omitempty"`
}
type card struct {
	Header   *cardHeader   `json:"header,omitempty"`
	Sections []cardSection `json:"sections,omitempty"`
}

type gchatMessage struct {
	Text  string `json:"text,omitempty"`
	Cards []card `json:"cards"`
}

func (f *Forwarder) Forward(ctx context.Context, body am.AMWebhookBody) error {

	req := http.Request{
		Method: http.MethodPost,
		URL:    f.webhookURL,
		Body:   nil,
	}

	title := "Alerts " + body.Status
	alertNames := map[string]struct{}{}

	if alertName, hasName := body.CommonLabels["alertname"]; hasName {
		title += ": " + alertName
		alertNames[alertName] = struct{}{}
	}

	headerCard := card{
		Header: &cardHeader{
			Title:      title,
			ImageUrl:   promFavicon,
			ImageStyle: "IMAGE", // not an avatar (I think)
		},
	}

	c := card{
		Sections: make([]cardSection, 0, len(body.Alerts)+1),
	}
	mapToString := func(m map[string]string) string {
		kvs := strings.Builder{}
		for k, v := range m {
			fmt.Fprintf(&kvs, "%s: %s<br>", k, v)
		}
		return kvs.String()
	}
	// add the overall summary section
	summary := cardSection{
		Widgets: []cardWidget{{
			TextParagraph: &cardTextParagraph{Text: fmt.Sprintf("%d alerts %s for receiver %q <br> %d alerts truncated <br> <a href=\"%s\">%s</a>",
				len(body.Alerts), body.Status, body.Receiver, body.TruncatedAlerts,
				body.ExternalURL, body.ExternalURL)},
		}, {
			KeyValue: &cardKeyVal{
				TopLabel:         "Group Labels",
				Content:          mapToString(body.GroupLabels),
				ContentMultiline: true,
			},
		}, {
			KeyValue: &cardKeyVal{
				TopLabel:         "Common Annotations",
				Content:          mapToString(body.CommonAnnotations),
				ContentMultiline: true,
			},
		}, {
			KeyValue: &cardKeyVal{
				TopLabel:         "Common Labels",
				Content:          mapToString(body.CommonLabels),
				ContentMultiline: true,
			},
		},
		},
	}

	c.Sections = append(c.Sections, summary)

	for _, alert := range body.Alerts {
		alSec := cardSection{
			Widgets: []cardWidget{
				{
					TextParagraph: &cardTextParagraph{Text: fmt.Sprintf("%s<br>Status: %s<br>Starts: %s<br>Ends: %s",
						alert.Labels["alertname"],
						alert.Status, alert.StartsAt, alert.EndsAt)},
				}, {
					KeyValue: &cardKeyVal{
						TopLabel:         "Labels",
						Content:          mapToString(alert.Labels),
						ContentMultiline: true,
					},
				}, {
					KeyValue: &cardKeyVal{
						TopLabel:         "Annotations",
						Content:          mapToString(alert.Annotations),
						ContentMultiline: true,
					},
				}, {
					TextParagraph: &cardTextParagraph{Text: "GeneratorURL: <a href=\"" + alert.GeneratorURL + "\"> " + alert.GeneratorURL + "</a>"},
				},
			},
		}
		c.Sections = append(c.Sections, alSec)
		if alertName, hasName := alert.Labels["alertname"]; hasName {
			alertNames[alertName] = struct{}{}
		}
	}

	if len(alertNames) > 1 {
		ks := strings.Builder{}
		for k := range alertNames {
			ks.WriteString(k)
			ks.WriteString(", ")
		}
		ksS := ks.String()
		title += ": [" + ksS[:len(ksS)-2] + "]"
	}

	reqBody := gchatMessage{
		Text: fmt.Sprintf("%s; %d alerts %s for receiver %q", title,
			len(body.Alerts), body.Status, body.Receiver),
		Cards: []card{headerCard, c},
	}
	log.Printf("sending %+v to %s", reqBody, f.webhookURL)
	_, resp, postErr := jsonhttp.PostJSON[struct{}](ctx, f.client, &req, &reqBody)
	if postErr != nil {
		log.Printf("received response status: %s", resp.Status)
	}
	return postErr
}

func NewForwarder(client *http.Client, u string) (*Forwarder, error) {
	pURL, parErr := url.Parse(u)
	if parErr != nil {
		return nil, fmt.Errorf("error parsing URL %q: %w", u, parErr)
	}
	return &Forwarder{webhookURL: pURL, client: client}, nil
}
