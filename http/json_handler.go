package http

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
)

// JSONHandler implementations implement ServeJSON
type JSONHandler[I, O any] interface {
	ServeJSON(w http.ResponseWriter, req *http.Request, reqBody I) (O, error)
}

// FallbackHandler handles errors if there is a problem with the main request
type FallbackHandler interface {
	ServeHTTP(w http.ResponseWriter, r *http.Request, err error)
}

// Adapter adapts a JSONHandler into a net/http.Handler
// Deserializes the request body into type I before calling the inner handler
// Attempts to serialize the returned value from ServeJSON into a request.
// Calls a fallback handler if either the ServeJSON fails or marshaling/unmarshaling fails
type JSONAdapter[I, O any] struct {
	inner    JSONHandler[I, O]
	fallback FallbackHandler
}

// NewJSONAdapter constructs a JSONAdapter for use as an http.Handler
func NewJSONAdapter[I, O any](inner JSONHandler[I, O], fallback FallbackHandler) *JSONAdapter[I, O] {
	return &JSONAdapter[I, O]{
		inner:    inner,
		fallback: fallback,
	}
}

// ServeHTTP implements net/http.Handler
func (j *JSONAdapter[I, O]) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	b, bErr := io.ReadAll(r.Body)
	if bErr != nil {
		j.fallback.ServeHTTP(w, r, fmt.Errorf("failed to read body: %w", bErr))
		return
	}

	var in I

	if unmarErr := json.Unmarshal(b, &in); unmarErr != nil {
		j.fallback.ServeHTTP(w, r, fmt.Errorf("failed to unmarshal request into type %T: %w", in, unmarErr))
		return
	}

	log.Printf("received %+v (from %s)", in, string(b))

	out, handleErr := j.inner.ServeJSON(w, r, in)
	if handleErr != nil {
		j.fallback.ServeHTTP(w, r, fmt.Errorf("failed to run handler: %w", handleErr))
		return
	}

	encResp, encErr := json.Marshal(out)
	if encErr != nil {
		j.fallback.ServeHTTP(w, r, fmt.Errorf("failed to marshal response body into type %T: %w", out, encErr))
		return
	}

	// make sure to set the JSON Content-Type
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Content-Length", strconv.Itoa(len(encResp)))

	w.WriteHeader(http.StatusOK)

	// throw away the error for now, because we literally can't change anything at this point.
	w.Write(encResp)
}
