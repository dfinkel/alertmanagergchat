package http

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

// UnsuccessfulStatusErr indicates that the request failed with an HTTP status code > 299
type UnsuccessfulStatusErr struct {
	resp *http.Response
	body []byte
}

// Error implements error
func (u *UnsuccessfulStatusErr) Error() string {
	return fmt.Sprintf("response > 299: %s (%d)",
		u.resp.Status, u.resp.StatusCode)
}

// Resp returns the response that errored
func (u *UnsuccessfulStatusErr) Resp() *http.Response {
	return u.resp
}

// Body returns the body that errored
func (u *UnsuccessfulStatusErr) Body() []byte {
	return u.body
}

// UnmarshalJSONErr indicates a failure to unmarshal the value into type T
type UnmarshalJSONErr struct {
	body  []byte
	inner error
}

// Error implements error
func (u *UnmarshalJSONErr) Error() string {
	return fmt.Sprintf("failed to unmarshal body: %s", u.inner)
}

// Body returns the body that failed to deserialize
func (u *UnmarshalJSONErr) Body() []byte {
	return u.body
}

// Unwrap implements the errors package interface for unwrapping
func (u *UnmarshalJSONErr) Unwrap() error {
	return u.inner
}

// PostJSON marshals the payload into JSON and writes that to the request body,
// then deserializes the response into a struct of type T (if the response is a 2xx class status)
// If deserialization fails or the status is not in the 2xx class, may return
// nil for the response payload, in addition to a response object, and an error.
func PostJSON[T any](ctx context.Context, client *http.Client, inReq *http.Request, payload any) (*T, *http.Response, error) {

	pEnc, marErr := json.Marshal(payload)
	if marErr != nil {
		return nil, nil, fmt.Errorf("failed to marshal payload: %w", marErr)
	}

	// Do a deep copy of the request, with a new context
	req := inReq.Clone(ctx)

	// clobber the body
	req.Body = io.NopCloser(bytes.NewReader(pEnc))
	req.Method = http.MethodPost
	if req.Header == nil {
		req.Header = http.Header{}
	}
	req.Header.Set("Content-Type", "application/json")
	req.ContentLength = int64(len(pEnc))

	resp, reqErr := client.Do(req)
	if reqErr != nil {
		return nil, nil, fmt.Errorf("request failed: %w", reqErr)
	}

	defer resp.Body.Close()
	bVal, readErr := io.ReadAll(resp.Body)
	if readErr != nil {
		return nil, resp, fmt.Errorf("failed to read body: %w", readErr)
	}

	if resp.StatusCode > 299 {
		return nil, resp, &UnsuccessfulStatusErr{
			resp: resp,
			body: bVal,
		}
	}

	var rBody T

	unmarshalErr := json.Unmarshal(bVal, &rBody)
	if unmarshalErr != nil {
		return nil, resp, &UnmarshalJSONErr{
			body:  bVal,
			inner: unmarshalErr,
		}
	}

	return &rBody, resp, nil
}
